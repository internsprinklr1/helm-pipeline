import re

regexJavaAdvBool = re.compile(r'-XX:[+-].*')
regexJavaAdvParam = re.compile(r'-XX:[a-z|A-Z].*=.+')
regexJavaNonStandardBool = re.compile(r'-X[a-z]+')
regexJavaNonStandardParam = re.compile(r'-X[a-z]+[:=0-9].+')
regexJavaStandardParam = re.compile(r'-.+') # Do not parse if starts with -D, Check for this regex match last, check for : to separate name and value, then =. 


print(regexJavaStandardParam.fullmatch("-Dsafjklsdnf.fdsjng.dsfsd=sdfds"))