import yaml, argparse, os
from yaml.loader import BaseLoader
from collections import defaultdict

class UnitHandler():

    @staticmethod
    def getHeapSizeInGigabytes(heapSizeInYaml: str) -> float:
        """
        Unit will be one of k(kilobytes), m(megabytes), or g(gigabytes). 
        Convert to g for ratio and offheap calculation from total memory.
        Input string is of the type 48g

        """
        try:
            unit = heapSizeInYaml[-1:]
            if unit == "g":
                return float("{:.4f}".format(float(heapSizeInYaml[:-1])))
            elif unit == "m":
                return float("{:.4f}".format(float(heapSizeInYaml[:-1])/1024))
            elif unit == "k":
                return float("{:.4f}".format(float(heapSizeInYaml[:-1])/(1024 * 1024)))
            else:
                raise ValueError("Heap size defined in invalid format.")
        except Exception as e:
            raise ValueError("Heap size defined in invalid format.")

    @staticmethod
    def getCPUsInCores(CPUCoresInYaml: str) -> float:
        """
        Unit for CPU will be either just a number, which would mean the number of cores, or suffixed with 'm', meaning milli cores.
        Convert milli cores to cores for ratio calculation
        Input strings are of the type: 4, 0.4, 400m

        """
        CPUCoresInYaml = CPUCoresInYaml.strip("\'\"")
        try:
            if CPUCoresInYaml[-1] == "m":
                return float("{:.4f}".format(float(CPUCoresInYaml[:-1])/1000))
            else:
                return float(CPUCoresInYaml)
        except Exception as e:
            raise ValueError("CPU cores defined in invalid format.")

    @staticmethod
    def getMemorySizeInGigabytes(memorySizeInYaml: str) -> float:
        """
        Unit for memory can be:
        b(1 bit), kb(1000 bits)
        m(1 millibyte), B(8 bits/1 byte), Ki(1024 bits), M(10^6 bytes), Mi(2^20 bytes), G(10^9 bytes), Gi(2^30 bytes)
        Convert memory to G for ratio calculation
        Input strings are of the type: 64Mi, 128Mi, 58G
        """
        memorySizeInYaml = memorySizeInYaml.strip("\'\"")
        try:
            if memorySizeInYaml[-2:] == "kb":
                return float("{:.4f}".format(float(memorySizeInYaml[:-2]) * 1.25e-7))
            elif memorySizeInYaml[-1] == "b":
                return float("{:.4f}".format(float(memorySizeInYaml[:-1]) * 1.25e-10))
            elif memorySizeInYaml[-1] == "m":
                return float("{:.4f}".format(float(memorySizeInYaml[:-1]) * 1e-12))
            elif memorySizeInYaml[-1] == "B":
                return float("{:.4f}".format(float(memorySizeInYaml[:-1]) * 1e-9))
            elif memorySizeInYaml[-2:] == "Ki":
                return float("{:.4f}".format(float(memorySizeInYaml[:-2]) * 1.28e-7))
            elif memorySizeInYaml[-1] == "M":
                return float("{:.4f}".format(float(memorySizeInYaml[:-1]) * 1e-3))
            elif memorySizeInYaml[-2:] == "Mi":
                return float("{:.4f}".format(float(memorySizeInYaml[:-2]) * 0.00104858))
            elif memorySizeInYaml[-1] == "G":
                return float("{:.4f}".format(float(memorySizeInYaml[:-1])))
            elif memorySizeInYaml[-2:] == "Gi":
                return float("{:.4f}".format(float(memorySizeInYaml[:-2]) * 1.07374))
            else:
                raise ValueError("Memory size defined in invalid format.", )
        except Exception as e:
            raise ValueError("Memory size defined in invalid format.")

class YamlAttributes():
    def __init__(self) -> None:
        self.tuningParameters = {}
        self.machine = {}
        self.lowPriorityIssues = 0
        self.mediumPriorityIssues = 0
        self.highPriorityIssues = 0

def parseWithErrorOnDuplicateKeys(fp):
    # We deliberately define a fresh class inside the function,
    # because add_constructor is a class method and we don't want to
    # mutate pyyaml classes.
    class PreserveDuplicatesLoader(yaml.loader.Loader):
        pass

    def map_constructor(loader, node, deep=False):
        """Walk the mapping, recording any duplicate keys.

        """
        mapping = dict()
        for key_node, value_node in node.value:
            key = loader.construct_object(key_node, deep=deep)
            value = loader.construct_object(value_node, deep=deep)

            if key in mapping:
                raise AssertionError("Duplicate keys in yaml.")
            else:
                mapping[key] = value

        return mapping
    
    PreserveDuplicatesLoader.add_constructor(yaml.resolver.BaseResolver.DEFAULT_MAPPING_TAG, map_constructor)
    return yaml.load(fp, PreserveDuplicatesLoader)

def getCMDArgs():
    parser = argparse.ArgumentParser()
    parser.add_argument("--source_branch", required=False)
    parser.add_argument("--target_branch", required=False)
    parser.add_argument('--cost',
                    action='store_true')
    args = parser.parse_args()

    return args

def getParameters(parsedYaml: dict, fileAttributes: YamlAttributes, isCostCheck=False):
    getMemoryAndCPU(parsedYaml, fileAttributes, isCostCheck)
    
    if "javaOpts" in parsedYaml:
        parseJavaOpts(parsedYaml['javaOpts'], fileAttributes.tuningParameters)
    
    getOffHeapSize(fileAttributes.tuningParameters)
    
    getReplicasAndAutoScaleParams(parsedYaml, fileAttributes)

def getReplicasAndAutoScaleParams(parsedYaml: dict, fileAttributes: YamlAttributes):
    if "replicas" in parsedYaml:
        fileAttributes.tuningParameters['replicas'] = int(parsedYaml['replicas'])
    
    if "hpa" in parsedYaml:
        getAutoScaleParams(parsedYaml, fileAttributes.tuningParameters)

def getAutoScaleParams(parsedYaml: dict, tuningParameters: dict):
    # Get the boolean value for hpa
    if parsedYaml['hpa'] == "true" or parsedYaml['hpa'] == True:
        tuningParameters['hpa'] = True
        # Get the min and max replica count
        if "autoscale" in parsedYaml:
            if "minReplicaCount" in parsedYaml['autoscale']:
                tuningParameters['minReplicaCount'] = int(parsedYaml['autoscale']['minReplicaCount'])
            if "maxReplicaCount" in parsedYaml['autoscale']:
                tuningParameters['maxReplicaCount'] = int(parsedYaml['autoscale']['maxReplicaCount'])
    else:
        tuningParameters['hpa'] = False

def parseJavaOpts(javaOpts: str, tuningParameters: dict):
    """
    Gets the xmx, xms, and max direct memory size.
    Also checks for repeated parameters. Splits the javaOpts string on `space`, then for each paramter, the parameter name
    and value are separated by: 1. '=', 2. ':', 3. Numeric characters.
    Might need to be changed if javaOpts specification is changed.
    """
    javaOpts = javaOpts.split(' ')
    optsMap = {}
    for opt in javaOpts:
        # Corner case for differently defined parameter
        if opt[:7] == "-Xloggc":
            parameterName = "-Xloggc"
            if parameterName in optsMap:
                raise AssertionError(f"javaOpts parameter {parameterName} defined multiple times.")
            else:
                optsMap[parameterName] = opt[8:]
                continue
        index = opt.find('=')
        if index != -1:
            # Parameter name is to the left of index
            parameterName = opt[:index]
            if parameterName in optsMap:
                raise AssertionError(f"javaOpts parameter {parameterName} defined multiple times.")
            else:
                optsMap[parameterName] = opt[index+1:]
                continue
        elif opt.find(":+") != -1:
            # toggle parameter
            index = opt.find(":+")
            parameterName = opt[index+2:]
            if parameterName in optsMap:
                raise AssertionError(f"javaOpts parameter {parameterName} defined multiple times.")
            else:
                optsMap[parameterName] = True
                continue
        elif opt.find(":-") != -1:
            # toggle parameter
            index = opt.find(":-")
            parameterName = opt[index+2:]
            if parameterName in optsMap:
                raise AssertionError(f"javaOpts parameter {parameterName} defined multiple times.")
            else:
                optsMap[parameterName] = False
                continue
        else:
        # Parameter name will be to the left of first alphanum
            for i in range(len(opt)):
                if opt[i].isdigit():
                    # The parameter name is to the left of i from the beginning
                    parameterName = opt[:i]
                    if parameterName in optsMap:
                        raise AssertionError(f"javaOpts parameter {parameterName} defined multiple times.")
                    else:
                        optsMap[parameterName] = opt[i:]
                        break
                    
    # Convert required parameters to proper units
    if '-Xms' in optsMap:
        tuningParameters['minHeapSize'] = UnitHandler.getHeapSizeInGigabytes(optsMap['-Xms'])
    if '-Xmx' in optsMap:
        tuningParameters['maxHeapSize'] = UnitHandler.getHeapSizeInGigabytes(optsMap['-Xmx'])
    if "-XX:MaxDirectMemorySize" in optsMap:
        tuningParameters['maxDirectMemory'] = UnitHandler.getHeapSizeInGigabytes(optsMap['-XX:MaxDirectMemorySize'])
    
    if 'memoryLimit' in tuningParameters:
        if not 'minHeapSize' in tuningParameters:
            tuningParameters['minHeapSize'] = float("{:.4f}".format(tuningParameters['memoryLimit'] / 64))
        if not 'maxHeapSize' in tuningParameters:
            tuningParameters['maxHeapSize'] = float("{:.4f}".format(tuningParameters['memoryLimit'] / 4))

def getMemoryAndCPU(parsedYaml: dict, fileAttributes: YamlAttributes, isCostCheck=False):
    resources = {}
    if "resources" in parsedYaml:
        resources = parsedYaml['resources']
    else:
        # Handle the case where limits and resources are at indent 0
        if "limits" in parsedYaml:
            resources['limits'] = parsedYaml['limits']
        if "requests" in parsedYaml:
            resources['requests'] = parsedYaml['requests']

    (hasMemoryLimit, hasCPULimit) = (False, False)
    if "limits" in resources:
        if "memory" in resources['limits']:
            hasMemoryLimit = True
            fileAttributes.tuningParameters['memoryLimit'] = UnitHandler.getMemorySizeInGigabytes(resources['limits']['memory'])
        if "cpu" in resources['limits']:
            hasCPULimit = True
            fileAttributes.tuningParameters['CPULimit'] = UnitHandler.getCPUsInCores(resources['limits']['cpu'])
    
    if not hasCPULimit or not hasMemoryLimit:
        if isCostCheck:
            machine = fileAttributes.machine
            if 'memory' in machine and 'cpu' in machine:
                fileAttributes.tuningParameters['CPULimit'] = UnitHandler.getCPUsInCores(machine['cpu'])
                fileAttributes.tuningParameters['memoryLimit'] = UnitHandler.getMemorySizeInGigabytes(machine['memory'])
        else:
            raise AssertionError("CPU and Memory limit must be defined.")

    if "requests" in resources:
        if "memory" in resources['requests']:
            fileAttributes.tuningParameters['memoryRequested'] = UnitHandler.getMemorySizeInGigabytes(resources['requests']['memory'])
        else:
            fileAttributes.tuningParameters['memoryRequested'] = fileAttributes.tuningParameters['memoryLimit']
    
        if "cpu" in resources['requests']:
            fileAttributes.tuningParameters['CPURequested'] = UnitHandler.getCPUsInCores(resources['requests']['cpu'])
        else:
            fileAttributes.tuningParameters['CPURequested'] = fileAttributes.tuningParameters['CPULimit']

def getOffHeapSize(tuningParameters: dict):
    try:
        tuningParameters['offHeapSize'] = float("{:.4f}".format(tuningParameters['memoryRequested'] - tuningParameters['maxHeapSize']))
    except Exception as e:
        print("[!] Insufficient specifications in the yaml file for calculating off heap size.")

def getNodeParams(parsedYaml: dict, tuningParameters: dict):
    if "k8sClusterName" in parsedYaml:
        tuningParameters['cluster'] = parsedYaml["k8sClusterName"]
    if "nodeSelector" in parsedYaml:
        tuningParameters['nodeSelectorKey'] = list(parsedYaml['nodeSelector'].keys())[0]
        tuningParameters['nodeSelectorValue'] = parsedYaml['nodeSelector'][tuningParameters['nodeSelectorKey']]

def calculateRatios(tuningParameters: dict):
    if 'memoryLimit' in tuningParameters and 'CPULimit' in tuningParameters:
        tuningParameters['memoryToCPURatio'] = float("{:.4f}".format(tuningParameters["memoryLimit"] / tuningParameters['CPULimit']))
    if 'maxHeapSize' in tuningParameters and 'offHeapSize' in tuningParameters:
        tuningParameters['heapToOffHeapRatio'] = float("{:.4f}".format(tuningParameters['maxHeapSize']/tuningParameters['offHeapSize']))


def performChecks(fileAttributes: YamlAttributes, configParams: dict):
    tuningParameters = fileAttributes.tuningParameters

    # Memory to cpu ratio should be within some threshold from powers of 2
    if 'memoryToCPURatio' in tuningParameters:
        actualRatio = tuningParameters['memoryToCPURatio']
        deviationThreshold = float(configParams['ratioDeviationPercentage'])/100
        powersOf2 = [1, 2, 4, 8, 16, 32]
        isThresholdSatisfied = False

        for power in powersOf2:
            deviation = abs(power-actualRatio)
            if deviation <= deviationThreshold * power:
                isThresholdSatisfied = True
                break
        
        if not isThresholdSatisfied:
            raise AssertionError(f"Memory to cpu ratio {actualRatio} is not within {deviationThreshold*100}% threshold from a power of 2.")
    
    # Memory to cpu ratio should be close to that of the assigned machine
    if 'memoryToCPURatio' in tuningParameters:
        machine = fileAttributes.machine
        if 'memory' in machine and 'cpu' in machine:
            memoryToCPURatioMachine = float("{:.4f}".format((float(UnitHandler.getMemorySizeInGigabytes(machine['memory'])/float(machine['cpu'])))))
            deviationThreshold = float(configParams['ratioDeviationPercentage']) / 100
            deviation = abs(memoryToCPURatioMachine - tuningParameters['memoryToCPURatio'])
            if deviation > deviationThreshold * memoryToCPURatioMachine:
                raise AssertionError(f"Memory to CPU ratio defined in values.yml: {tuningParameters['memoryToCPURatio']} does not meet the deviation threshold of {deviationThreshold*100} percent from the allocated machine ratio: {memoryToCPURatioMachine}.")

    # MaxDirectMemorySize < offHeapSize
    if 'maxDirectMemory' in tuningParameters and 'offHeapSize' in tuningParameters:
        if tuningParameters['maxDirectMemory'] > tuningParameters['offHeapSize']:
            raise AssertionError(f"MaxDirectMemorySize {tuningParameters['maxDirectMemory']} cannot be greater than offHeap size: {tuningParameters['offHeapSize']}")
        
    # CPU and memory requests < limit
    if 'memoryRequested' in tuningParameters and 'memoryLimit' in tuningParameters:
        if tuningParameters['memoryRequested'] > tuningParameters['memoryLimit']:
            raise AssertionError(f"Requested memory cannot be greater than memory limit.")

    if 'CPURequested' in tuningParameters and 'CPULimit' in tuningParameters:
        if tuningParameters['CPURequested'] > tuningParameters['CPULimit']:
            raise AssertionError(f"Requested cpu cannot be greater than cpu limit")
    
    # Xms == Xmx
    if 'minHeapSize' in tuningParameters and 'maxHeapSize' in tuningParameters:
        if tuningParameters['minHeapSize'] != tuningParameters['maxHeapSize']:
            raise AssertionError(f"Xms must be equal to Xmx.")
    
    # Replicas should be between min and max replicas defined in autoscale if hpa is true
    if "hpa" in tuningParameters and tuningParameters['hpa'] is True:
        if 'minReplicaCount' in tuningParameters and 'maxReplicaCount' in tuningParameters and 'replicas' in tuningParameters:
            if tuningParameters['replicas'] < tuningParameters['minReplicaCount'] or tuningParameters['replicas'] > tuningParameters['maxReplicaCount']:
                raise AssertionError("Replica count must be between minReplicaCount and maxReplicaCount")

    # Heap + offHeap < requested memory. 
    # From the scripts logic, just check offHeap > 0
    if 'offHeapSize' in tuningParameters:
        if tuningParameters['offHeapSize'] < 0:
            raise AssertionError("Xmx must be less than requested memory")
    
    # Off heap size should be less than 50% of requested memory
    if 'offHeapSize' in tuningParameters:
        if 'memoryRequested' in tuningParameters:
            if tuningParameters['offHeapSize'] > 0.5 * tuningParameters['memoryRequested']:
                raise AssertionError("Off heap size must be less than 50% of requested memory")
    # Cost Analysis
    performCostAnalysis(fileAttributes)

def performCostAnalysis(fileAttributes: YamlAttributes):
    machine = fileAttributes.machine
    tuningParameters = fileAttributes.tuningParameters
    if 'memoryToCPURatio' in tuningParameters and 'cpu' in machine and 'memory' in machine and 'price' in machine:
        replicaCount = 1
        if 'maxReplicaCount' in tuningParameters:
            replicaCount = tuningParameters['maxReplicaCount']
        elif 'replicas' in tuningParameters:
            replicaCount = tuningParameters['replicas']

        podRatio = tuningParameters['memoryToCPURatio']
        machineCPU = UnitHandler.getCPUsInCores(machine['cpu'])
        machineMemory = UnitHandler.getMemorySizeInGigabytes(machine['memory'])
        machineRatio = float("{:.4f}".format(machineMemory/machineCPU))
        if podRatio <= machineRatio:
            # CPU gets exhausted first, price according to cpu
            tuningParameters['podCost'] = replicaCount * tuningParameters['CPULimit'] * (machine['price']/machineCPU)
        else:
            # Memory gets exhausted first, price according to memory
            tuningParameters['podCost'] = replicaCount * tuningParameters['memoryLimit'] * (machine['price']/machineMemory)
        fileAttributes.tuningParameters = tuningParameters

def getMachine(configParams: dict, fileAttributes: YamlAttributes) -> bool:
    tuningParameters = fileAttributes.tuningParameters
    machines = []

    if 'cluster' in tuningParameters:
        if tuningParameters['cluster'] in configParams['clusters']:
            if 'nodeSelectorKey' in tuningParameters and tuningParameters['nodeSelectorKey'] in configParams['clusters'][tuningParameters['cluster']]:
                if 'nodeSelectorValue' in tuningParameters and tuningParameters['nodeSelectorValue'] in configParams['clusters'][tuningParameters['cluster']][tuningParameters['nodeSelectorKey']]:
                    machines = configParams['clusters'][tuningParameters['cluster']][tuningParameters['nodeSelectorKey']][tuningParameters['nodeSelectorValue']]

                    # Handle multiple machines here
                    # CPU to memory ratio would ideally be same for all the machines.
                    targetMachine = machines[0]
                    machineDict = {
                        'cpu': configParams['instanceTypes'][targetMachine]['cpu'],
                        'memory': configParams['instanceTypes'][targetMachine]['memory'],
                        'price': configParams['instanceTypes'][targetMachine]['price'],
                    }
                    # Set the price as the average of all
                    if len(machines) > 1:
                        price = 0
                        for machine in machines:
                            price += float("{:.4f}".format(float(configParams['instanceTypes'][machine]['price'])))

                        price = float("{:.4f}".format(price/len(machines)))
                        machineDict['price'] = price
                    fileAttributes.machine = machineDict
                    return True
    return False

def parseYamls(ymlName: str, isCostCheck=False) -> dict:
        changedFilesAttributes = {}
        # for line in lines:
        #     # Check if the file is a yaml file.
        #     ymlName = str(line)[:-1]
        #     if ymlName[-10:] == "values.yml" or ymlName[-11:] == "values.yaml":
        if os.path.exists(ymlName):
            with open(ymlName, 'r') as f:
                fileAttributes = YamlAttributes()
                print(f"[*] Parsing {ymlName}:")
                parsedYaml = parseWithErrorOnDuplicateKeys(f)
                if isCostCheck:
                    getNodeParams(parsedYaml, fileAttributes.tuningParameters)
                    if getMachine(configParams, fileAttributes):
                        getReplicasAndAutoScaleParams(parsedYaml, fileAttributes)
                        getMemoryAndCPU(parsedYaml, fileAttributes, isCostCheck=True)
                        calculateRatios(fileAttributes.tuningParameters)
                        performCostAnalysis(fileAttributes)
                else:
                    getNodeParams(parsedYaml, fileAttributes.tuningParameters)
                    getMachine(configParams, fileAttributes)
                    getParameters(parsedYaml, fileAttributes, isCostCheck)
                    calculateRatios(fileAttributes.tuningParameters)
                    performChecks(fileAttributes, configParams)
                changedFilesAttributes[ymlName] = fileAttributes
        return changedFilesAttributes

if __name__ == "__main__":
    args = getCMDArgs()
    # lines = None
    # with open("changed_files.txt", "r") as fp:
    #     lines = fp.readlines()
    
    with open("config.yml", "r") as fp:
        configParams = yaml.load(fp, Loader=BaseLoader)

    if args.cost:
        # Run for the target branch
        os.system(f'git checkout {args.target_branch}')
        targetBranchChangedFilesAttributes = parseYamls(lines, isCostCheck=True)
        os.system('git restore .')
        # Run for source branch
        os.system(f'git checkout {args.source_branch}')
        sourceBranchChangedFilesAttributes = parseYamls(lines, isCostCheck=True)

        for filename in sourceBranchChangedFilesAttributes.keys():
            if filename in targetBranchChangedFilesAttributes.keys():
                print(f"Cost analysis for {filename}")
                sourceBranchAttributes = sourceBranchChangedFilesAttributes[filename]
                targetBranchAttributes = targetBranchChangedFilesAttributes[filename]
                print(sourceBranchAttributes.tuningParameters)
                print(targetBranchAttributes.tuningParameters)
                if 'podCost' in sourceBranchAttributes.tuningParameters and 'podCost' in targetBranchAttributes.tuningParameters:
                    sourceBranchCost = sourceBranchAttributes.tuningParameters['podCost']
                    targetBranchCost = targetBranchAttributes.tuningParameters['podCost']
                    
                    if sourceBranchCost > targetBranchCost:
                        if sourceBranchCost-targetBranchCost > int(configParams['diffPriceThreshold']):
                            raise AssertionError(f"Cost difference {sourceBranchCost-targetBranchCost} exceeds diffPriceThreshold. Merge increases cost.")
                    elif sourceBranchCost < targetBranchCost:
                        if targetBranchCost-sourceBranchCost > int(configParams['diffPriceThreshold']):
                            raise AssertionError(f"Cost difference {targetBranchCost-sourceBranchCost} exceeds diffPriceThreshold. Merge decreases cost.")
            else:
                # May include new deployment
                sourceBranchAttributes = sourceBranchChangedFilesAttributes[filename]
                print(sourceBranchAttributes.tuningParameters)
                if 'podCost' in sourceBranchAttributes.tuningParameters:
                    if sourceBranchAttributes.tuningParameters['podCost'] > int(configParams['newDeploymentPriceThreshold']):
                        raise AssertionError(f"{filename} cost {sourceBranchAttributes.tuningParameters['podCost']} exceeds newDeploymentPriceThreshold.")
    else:
        sourceBranchChangedFilesAttributes = parseYamls("test-helms/9.yml", isCostCheck=False)
        for filename in sourceBranchChangedFilesAttributes.keys():
            print(f"{filename}: {sourceBranchChangedFilesAttributes[filename].tuningParameters}")