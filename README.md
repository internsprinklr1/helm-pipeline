# Heml Pipeline

## Checks performed:
- [X] Memory to CPU ratio should be close to powers of 2 (`ratioDeviationThreshold` in config file)
- [X] Memory to CPU ratio should be close to that of the assigned machine (`ratioDeviationThreshold` in config file)
- [X] MaxDirectMemorySize should be less than offHeapSize
- [X] CPU and memory requests should be lower than limit
- [X] Xmx should be less than requested memory 
- [X] Xms == Xmx
- [X] Cost Analysis (run asynchronously)
- [X] Repeated parameters
- [X] Upper limit on off heap size 50% of requested memory

## How to run the script:
1. The script has two **required files** to run successfully:
    * `run_checks.py`: The python script file.
    * `config.yml` : Contains the configurations and machine definitions.
    * `requirements.txt`: Contains the dependencies for the script to run.
2. Both of the above files need to be in the **same directory**, preferably in the root of the repository.
3. The Gitlab script to run the script is defined in `.gitlab-ci.yml`. The script is as follows:
    ```yaml
    perform-checks:
    stage: test
    script:
    - git fetch -a
    - git pull origin $CI_MERGE_REQUEST_SOURCE_BRANCH_NAME
    - pip install -r requirements.txt
    - git diff origin/$CI_MERGE_REQUEST_TARGET_BRANCH_NAME --name-only > changed_files.txt
    - python3 run_checks.py
    ```
    * Here `perform-checks` is the name of the job, which can be renamed to anything.
    * `stage:test` specifies the stage of the CI/CD pipeline, which can be changed to anything.
    * The five commands in the `script` section perform the following:
        1. Fetch the branches for comparision.
        2. Merge any changes in the remote branch with the local branch.
        3. Install the dependencies in the virtual environment.
        4. Compares the state of the `source` (current) branch with respect to the `target` branch and write the `path` to the changed in a files called `changed_files.txt`
        5. Runs the script.
4. The above script shoudld be placed in the `.gitlab-ci.yml` file of the repository for which the checks need to be performed.
5. The output of the job can be seen in the runner, along with the errors in the `yaml` files.