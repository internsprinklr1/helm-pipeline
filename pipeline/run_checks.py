import yaml, argparse, os, re, sys
from yaml.loader import BaseLoader

class YamlAttributes():
    def __init__(self, filename:str) -> None:
        self.tuningParameters = {}
        self.machine = {}
        self.errors = []
        try:
            splittedFilename = filename.split('/')
            self.filename = splittedFilename[1] + ", " + splittedFilename[3]
        except:
            self.filename = filename

class UnitHandler():

    @staticmethod
    def getHeapSizeInGigabytes(optsMap: dict, fileAttributes: YamlAttributes, parameterName: str) -> float:
        """
        Unit will be one of k(kilobytes), m(megabytes), or g(gigabytes). 
        Convert to g for ratio and offheap calculation from total memory.
        Input string is of the type 48g

        """
        try:
            heapSizeInYaml = optsMap[parameterName]
            unit = heapSizeInYaml[-1:]
            if unit == "g":
                return float("{:.4f}".format(float(heapSizeInYaml[:-1])))
            elif unit == "m":
                return float("{:.4f}".format(float(heapSizeInYaml[:-1])/1024))
            elif unit == "k":
                return float("{:.4f}".format(float(heapSizeInYaml[:-1])/(1024 * 1024)))
            else:
                fileAttributes.errors.append(f"{len(fileAttributes.errors)+1}: javaOpts parameter {parameterName} defined in invalid format.")
                return None
        except Exception as e:
                fileAttributes.errors.append(f"{len(fileAttributes.errors)+1}: javaOpts parameter {parameterName} defined in invalid format.")
                return None

    @staticmethod
    def getCPUsInCores(CPUCoresInYaml: str, fileAttributes: YamlAttributes) -> float:
        """
        Unit for CPU will be either just a number, which would mean the number of cores, or suffixed with 'm', meaning milli cores.
        Convert milli cores to cores for ratio calculation
        Input strings are of the type: 4, 0.4, 400m

        """
        try:
            return float(CPUCoresInYaml)
        except:
            CPUCoresInYaml = CPUCoresInYaml.strip("\'\"")
            try:
                if CPUCoresInYaml[-1] == "m":
                    return float("{:.4f}".format(float(CPUCoresInYaml[:-1])/1000))
                else:
                    return float(CPUCoresInYaml)
            except Exception as e:
                raise ValueError(f"Error in {fileAttributes.filename}: CPU cores defined in invalid format.")

    @staticmethod
    def getMemorySizeInGigabytes(memorySizeInYaml: str, fileAttributes: YamlAttributes) -> float:
        """
        Unit for memory can be:
        b(1 bit), kb(1000 bits)
        m(1 millibyte), B(8 bits/1 byte), Ki(1024 bits), M(10^6 bytes), Mi(2^20 bytes), G(10^9 bytes), Gi(2^30 bytes)
        Convert memory to G for ratio calculation
        Input strings are of the type: 64Mi, 128Mi, 58G
        """
        try:
            return float(memorySizeInYaml)
        except:
            memorySizeInYaml = memorySizeInYaml.strip("\'\"")
            try:
                if memorySizeInYaml[-2:] == "kb":
                    return float("{:.4f}".format(float(memorySizeInYaml[:-2]) * 1.25e-7))
                elif memorySizeInYaml[-1] == "b":
                    return float("{:.4f}".format(float(memorySizeInYaml[:-1]) * 1.25e-10))
                elif memorySizeInYaml[-1] == "m":
                    return float("{:.4f}".format(float(memorySizeInYaml[:-1]) * 1e-12))
                elif memorySizeInYaml[-1] == "B":
                    return float("{:.4f}".format(float(memorySizeInYaml[:-1]) * 1e-9))
                elif memorySizeInYaml[-2:] == "Ki":
                    return float("{:.4f}".format(float(memorySizeInYaml[:-2]) * 1.28e-7))
                elif memorySizeInYaml[-1] == "M":
                    return float("{:.4f}".format(float(memorySizeInYaml[:-1]) * 1e-3))
                elif memorySizeInYaml[-2:] == "Mi":
                    return float("{:.4f}".format(float(memorySizeInYaml[:-2]) * 0.00104858))
                elif memorySizeInYaml[-1] == "G":
                    return float("{:.4f}".format(float(memorySizeInYaml[:-1])))
                elif memorySizeInYaml[-2:] == "Gi":
                    return float("{:.4f}".format(float(memorySizeInYaml[:-2]) * 1.07374))
                else:
                    raise ValueError(f"Error in {fileAttributes.filename}: Memory size defined in invalid format.", )
            except Exception as e:
                raise ValueError(f"Error in {fileAttributes.filename}: Memory size defined in invalid format.")

def parseWithErrorOnDuplicateKeys(fp, fileAttributes: YamlAttributes):
    # We deliberately define a fresh class inside the function,
    # because add_constructor is a class method and we don't want to
    # mutate pyyaml classes.
    class PreserveDuplicatesLoader(yaml.loader.Loader):
        pass

    def map_constructor(loader, node, deep=False):
        """Walk the mapping, recording any duplicate keys.

        """
        mapping = dict()
        for key_node, value_node in node.value:
            key = loader.construct_object(key_node, deep=deep)
            value = loader.construct_object(value_node, deep=deep)

            if key in mapping:
                mapping[key] = value
                fileAttributes.errors.append(f"{len(fileAttributes.errors)+1}: Duplicate key: {key}")
            else:
                mapping[key] = value

        return mapping
    
    PreserveDuplicatesLoader.add_constructor(yaml.resolver.BaseResolver.DEFAULT_MAPPING_TAG, map_constructor)
    return yaml.load(fp, PreserveDuplicatesLoader)

def getCMDArgs():
    parser = argparse.ArgumentParser()
    parser.add_argument("--source_branch", required=False)
    parser.add_argument("--target_branch", required=False)
    parser.add_argument('--cost',
                    action='store_true')
    args = parser.parse_args()

    return args

def getParameters(parsedYaml: dict, fileAttributes: YamlAttributes, isCostCheck=False):
    getMemoryAndCPU(parsedYaml, fileAttributes, isCostCheck)
    
    if "javaOpts" in parsedYaml:
        parseJavaOpts(parsedYaml['javaOpts'], fileAttributes)
    
    getOffHeapSize(fileAttributes.tuningParameters)
    
    getReplicasAndAutoScaleParams(parsedYaml, fileAttributes)

def getReplicasAndAutoScaleParams(parsedYaml: dict, fileAttributes: YamlAttributes):
    if "replicas" in parsedYaml:
        fileAttributes.tuningParameters['replicas'] = int(parsedYaml['replicas'])
    
    if "hpa" in parsedYaml:
        getAutoScaleParams(parsedYaml, fileAttributes.tuningParameters)

def getAutoScaleParams(parsedYaml: dict, tuningParameters: dict):
    # Get the boolean value for hpa
    if parsedYaml['hpa'] == "true" or parsedYaml['hpa'] == True:
        tuningParameters['hpa'] = True
        # Get the min and max replica count
        if "autoscale" in parsedYaml:
            if "minReplicaCount" in parsedYaml['autoscale']:
                tuningParameters['minReplicaCount'] = int(parsedYaml['autoscale']['minReplicaCount'])
            if "maxReplicaCount" in parsedYaml['autoscale']:
                tuningParameters['maxReplicaCount'] = int(parsedYaml['autoscale']['maxReplicaCount'])
    else:
        tuningParameters['hpa'] = False

def handleJavaOpt(fileAttributes: YamlAttributes, parameterName: str, parameterValue: any, optsMap: dict,):
    if parameterName in optsMap:
        fileAttributes.errors.append(f"{len(fileAttributes.errors)+1}: javaOpts parameter {parameterName} defined multiple times.")
    else:
        optsMap[parameterName] = parameterValue

def getOptOrError(opt: str, optsMap: dict, fileAttributes: YamlAttributes):
    regexJavaAdvBool = re.compile(r'-XX:[+-].*')
    regexJavaAdvParam = re.compile(r'-XX:[a-z|A-Z].*=.+')
    regexJavaNonStandardBool = re.compile(r'-X[a-z]+')
    regexJavaNonStandardParam = re.compile(r'-X[a-z]+[:=0-9].+')
    regexJavaStandardParam = re.compile(r'-.+')
    
    if regexJavaAdvBool.fullmatch(opt) is not None:
        matchedString = regexJavaAdvBool.fullmatch(opt).string
        paramName = matchedString[5:]
        paramValue = True if matchedString[4] == '+' else False
        handleJavaOpt(fileAttributes, paramName, paramValue, optsMap)
    elif regexJavaAdvParam.fullmatch(opt) is not None:
        matchedString = regexJavaAdvParam.fullmatch(opt).string
        index = matchedString.find('=')
        paramName = matchedString[:index]
        paramValue = matchedString[index+1:]
        handleJavaOpt(fileAttributes, paramName, paramValue, optsMap)
    elif regexJavaNonStandardBool.fullmatch(opt) is not None:
        matchedString = regexJavaNonStandardBool.fullmatch(opt).string
        paramName = matchedString
        paramValue = True
        handleJavaOpt(fileAttributes, paramName, paramValue, optsMap)
    elif regexJavaNonStandardParam.fullmatch(opt) is not None:
        matchedString = regexJavaNonStandardParam.fullmatch(opt).string
        paramName = None
        paramValue = None
        index = -1
        if ':' in matchedString:
            index = matchedString.find(':')
            paramValue = matchedString[index+1:]
        elif '=' in matchedString:
            index = matchedString.find('=')
            paramValue = matchedString[index+1:]
        else:
            # Get index of first digit
            for i in range(len(matchedString)):
                if matchedString[i].isnumeric():
                    index = i
                    break
            paramValue = matchedString[index:]
        paramName = matchedString[:index]
        handleJavaOpt(fileAttributes, paramName, paramValue, optsMap)
    elif regexJavaStandardParam.fullmatch(opt) is not None:
        matchedString = regexJavaStandardParam.fullmatch(opt).string
        paramName = None
        paramValue = None
        # Do not parse if starts with -D, Check for this regex match last, check for : to separate name and value. 
        if not matchedString[1] == 'D':
            if ':' in matchedString:
                index = matchedString.find(':')
                paramName = matchedString[:index]
                paramValue = matchedString[index+1:]
            else:
                paramName = matchedString
                paramValue = True
            handleJavaOpt(fileAttributes, paramName, paramValue, optsMap)
    else:
        fileAttributes.errors.append(f"{len(fileAttributes.errors)+1}: Invalid javaOpts parameter {opt}")

def verifyInvalidOpts(optsMap: dict, fileAttributes: YamlAttributes):
    for opt in optsMap.keys():
        if opt not in validJavaOpts:
            fileAttributes.errors.append(f"{len(fileAttributes.errors)+1}: Invalid javaOpts parameter {opt}")

def parseJavaOpts(javaOpts: str, fileAttributes: YamlAttributes):
    """
    Gets the xmx, xms, and max direct memory size.
    Also checks for repeated parameters. Splits the javaOpts string on `space`, then for each paramter, the parameter name
    and value are separated by: 1. '=', 2. ':', 3. Numeric characters.
    Might need to be changed if javaOpts specification is changed.
    """
    tuningParameters = fileAttributes.tuningParameters
    javaOpts = javaOpts.split(' ')
    optsMap = {}
    for opt in javaOpts:
        opt = opt.strip()
        if len(opt) > 0:
            getOptOrError(opt, optsMap, fileAttributes)

    # print(optsMap)

    verifyInvalidOpts(optsMap, fileAttributes)
    # Convert required parameters to proper units
    if '-Xms' in optsMap:
        size = UnitHandler.getHeapSizeInGigabytes(optsMap, fileAttributes, '-Xms')
        if size:
            tuningParameters['minHeapSize'] = size
    if '-Xmx' in optsMap:
        size = UnitHandler.getHeapSizeInGigabytes(optsMap, fileAttributes, '-Xmx')
        if size:
            tuningParameters['maxHeapSize'] = size
    if "-XX:MaxDirectMemorySize" in optsMap:
        size = UnitHandler.getHeapSizeInGigabytes(optsMap, fileAttributes, '-XX:MaxDirectMemorySize')
        if size:
            tuningParameters['maxDirectMemory'] = size
    
    if 'memoryLimit' in tuningParameters:
        if not 'minHeapSize' in tuningParameters:
            tuningParameters['minHeapSize'] = float("{:.4f}".format(tuningParameters['memoryLimit'] / 64))
        if not 'maxHeapSize' in tuningParameters:
            tuningParameters['maxHeapSize'] = float("{:.4f}".format(tuningParameters['memoryLimit'] / 4))

def getMemoryAndCPU(parsedYaml: dict, fileAttributes: YamlAttributes, isCostCheck=False):
    resources = {}
    if "resources" in parsedYaml:
        resources = parsedYaml['resources']
    else:
        # Handle the case where limits and resources are at indent 0
        if "limits" in parsedYaml:
            resources['limits'] = parsedYaml['limits']
        if "requests" in parsedYaml:
            resources['requests'] = parsedYaml['requests']

    (hasMemoryLimit, hasCPULimit) = (False, False)
    if "limits" in resources:
        if resources['limits']:
            if "memory" in resources['limits']:
                hasMemoryLimit = True
                fileAttributes.tuningParameters['memoryLimit'] = UnitHandler.getMemorySizeInGigabytes(resources['limits']['memory'], fileAttributes)
            if "cpu" in resources['limits']:
                hasCPULimit = True
                fileAttributes.tuningParameters['CPULimit'] = UnitHandler.getCPUsInCores(resources['limits']['cpu'], fileAttributes)
    
    if not hasCPULimit or not hasMemoryLimit:
            machine = fileAttributes.machine
            if 'memory' in machine and 'cpu' in machine:
                fileAttributes.tuningParameters['CPULimit'] = UnitHandler.getCPUsInCores(machine['cpu'], fileAttributes)
                fileAttributes.tuningParameters['memoryLimit'] = UnitHandler.getMemorySizeInGigabytes(machine['memory'], fileAttributes)

    if "requests" in resources:
        if resources['requests']:
            if "memory" in resources['requests']:
                fileAttributes.tuningParameters['memoryRequested'] = UnitHandler.getMemorySizeInGigabytes(resources['requests']['memory'], fileAttributes)
            else:
                fileAttributes.tuningParameters['memoryRequested'] = 0
    
            if "cpu" in resources['requests']:
                fileAttributes.tuningParameters['CPURequested'] = UnitHandler.getCPUsInCores(resources['requests']['cpu'], fileAttributes)
            else:
                fileAttributes.tuningParameters['CPURequested'] = 0
        else:
            fileAttributes.errors.append(f"{len(fileAttributes.errors)+1}: Memory and CPU requests must be defined.")

def getOffHeapSize(tuningParameters: dict):
    if 'memoryRequested' in tuningParameters and 'maxHeapSize' in tuningParameters:
        tuningParameters['offHeapSize'] = float("{:.4f}".format(tuningParameters['memoryRequested'] - tuningParameters['maxHeapSize']))

def getNodeParams(parsedYaml: dict, tuningParameters: dict):
    if "k8sClusterName" in parsedYaml:
        tuningParameters['cluster'] = parsedYaml["k8sClusterName"]
    if "nodeSelector" in parsedYaml:
        tuningParameters['nodeSelectorKey'] = list(parsedYaml['nodeSelector'].keys())[0]
        tuningParameters['nodeSelectorValue'] = parsedYaml['nodeSelector'][tuningParameters['nodeSelectorKey']]

def calculateRatios(fileAttributes: YamlAttributes):
    tuningParameters = fileAttributes.tuningParameters
    if 'memoryRequested' in tuningParameters and 'CPURequested' in tuningParameters:
        if tuningParameters['memoryRequested'] > 0 and tuningParameters['CPURequested'] > 0:
            tuningParameters['memoryToCPURatio'] = float("{:.4f}".format(tuningParameters["memoryRequested"] / tuningParameters['CPURequested']))
        elif tuningParameters['memoryRequested'] > 0 and tuningParameters['CPURequested'] <= 0:
            tuningParameters['limitingMemory'] = tuningParameters['memoryRequested']
        elif tuningParameters['memoryRequested'] <= 0 and tuningParameters['CPURequested'] > 0:
            tuningParameters['limitingCPU'] = tuningParameters['CPURequested']
        else:
            fileAttributes.errors.append(f"{len(fileAttributes.errors)+1}: Memory and CPU requests must be defined.")

    if 'maxHeapSize' in tuningParameters and 'offHeapSize' in tuningParameters:
        if tuningParameters['offHeapSize'] <= 0:
            fileAttributes.errors.append(f"{len(fileAttributes.errors)+1}: Off heap size(memory request - xmx) must be greater than 0")
        else:
            tuningParameters['heapToOffHeapRatio'] = float("{:.4f}".format(tuningParameters['maxHeapSize']/tuningParameters['offHeapSize']))


def performChecks(fileAttributes: YamlAttributes, configParams: dict):
    tuningParameters = fileAttributes.tuningParameters

    # Memory to cpu ratio should be within some threshold from powers of 2
    if 'memoryToCPURatio' in tuningParameters:
        actualRatio = tuningParameters['memoryToCPURatio']
        deviationThreshold = float(configParams['ratioDeviationPercentage'])/100
        powersOf2 = [1, 2, 4, 8, 16, 32]
        isThresholdSatisfied = False

        for power in powersOf2:
            deviation = abs(power-actualRatio)
            if deviation <= deviationThreshold * power:
                isThresholdSatisfied = True
                break
        
        if not isThresholdSatisfied:
            fileAttributes.errors.append(f"{len(fileAttributes.errors)+1}: Memory to cpu ratio: {actualRatio} with cpu: {tuningParameters['CPURequested']} and memory: {tuningParameters['memoryRequested']} is not within {deviationThreshold*100}% threshold from a power of 2. ")
            # raise AssertionError(f"Memory to cpu ratio {actualRatio} is not within {deviationThreshold*100}% threshold from a power of 2.")
    
    # Memory to cpu ratio should be close to that of the assigned machine
    if 'memoryToCPURatio' in tuningParameters:
        machine = fileAttributes.machine
        if 'memory' in machine and 'cpu' in machine:
            memoryToCPURatioMachine = float("{:.4f}".format((float(UnitHandler.getMemorySizeInGigabytes(machine['memory'], fileAttributes)/float(machine['cpu'])))))
            deviationThreshold = float(configParams['ratioDeviationPercentage']) / 100
            deviation = abs(memoryToCPURatioMachine - tuningParameters['memoryToCPURatio'])
            if deviation > deviationThreshold * memoryToCPURatioMachine:
                fileAttributes.errors.append(f"{len(fileAttributes.errors)+1}: Memory to CPU ratio: {tuningParameters['memoryToCPURatio']} with cpu: {tuningParameters['CPURequested']} and memory: {tuningParameters['memoryRequested']} does not meet the deviation threshold of {deviationThreshold*100} percent from the allocated machine ratio: {memoryToCPURatioMachine} with cpu: {machine['cpu']} and memory: {machine['memory']}.")
                # raise AssertionError(f"Memory to CPU ratio defined in values.yml: {tuningParameters['memoryToCPURatio']} does not meet the deviation threshold of {deviationThreshold*100} percent from the allocated machine ratio: {memoryToCPURatioMachine}.")

    # MaxDirectMemorySize < offHeapSize
    if 'maxDirectMemory' in tuningParameters and 'offHeapSize' in tuningParameters:
        if tuningParameters['maxDirectMemory'] > tuningParameters['offHeapSize']:
            fileAttributes.errors.append(f"{len(fileAttributes.errors)+1}: MaxDirectMemorySize {tuningParameters['maxDirectMemory']} cannot be greater than offHeap(memory requested - Xmx) size: {tuningParameters['offHeapSize']}")
            # raise AssertionError(f"MaxDirectMemorySize {tuningParameters['maxDirectMemory']} cannot be greater than offHeap size: {tuningParameters['offHeapSize']}")
        
    # CPU and memory requests < limit
    if 'memoryRequested' in tuningParameters and 'memoryLimit' in tuningParameters:
        if tuningParameters['memoryRequested'] > tuningParameters['memoryLimit']:
            fileAttributes.errors.append(f"{len(fileAttributes.errors)+1}: Requested memory cannot be greater than memory limit.")
            # raise AssertionError(f"Requested memory cannot be greater than memory limit.")

    if 'CPURequested' in tuningParameters and 'CPULimit' in tuningParameters:
        if tuningParameters['CPURequested'] > tuningParameters['CPULimit']:
            fileAttributes.errors.append(f"{len(fileAttributes.errors)+1}: Requested cpu cannot be greater than cpu limit.")
            # raise AssertionError(f"Requested cpu cannot be greater than cpu limit")
    
    # Xms == Xmx
    if 'minHeapSize' in tuningParameters and 'maxHeapSize' in tuningParameters:
        if tuningParameters['minHeapSize'] != tuningParameters['maxHeapSize']:
            fileAttributes.errors.append(f"{len(fileAttributes.errors)+1}: Xms must be equal to Xmx.")
            # raise AssertionError(f"Xms must be equal to Xmx.")
    
    # Replicas should be between min and max replicas defined in autoscale if hpa is true
    # if "hpa" in tuningParameters and tuningParameters['hpa'] is True:
    #     if 'minReplicaCount' in tuningParameters and 'maxReplicaCount' in tuningParameters and 'replicas' in tuningParameters:
    #         if tuningParameters['replicas'] < tuningParameters['minReplicaCount'] or tuningParameters['replicas'] > tuningParameters['maxReplicaCount']:
    #             fileAttributes.errors.append(f"{len(fileAttributes.errors)+1}: Replica count must be between minReplicaCount and maxReplicaCount")
                # raise AssertionError("Replica count must be between minReplicaCount and maxReplicaCount")

    # Heap + offHeap < requested memory. 
    # From the scripts logic, just check offHeap > 0
    if 'offHeapSize' in tuningParameters:
        if tuningParameters['offHeapSize'] < 0:
            fileAttributes.errors.append(f"{len(fileAttributes.errors)+1}: Xmx must be less than requested memory")
            # raise AssertionError("Xmx must be less than requested memory")
    
    # Off heap size should be less than 50% of requested memory
    if 'offHeapSize' in tuningParameters:
        if 'memoryRequested' in tuningParameters:
            if tuningParameters['offHeapSize'] > 0.5 * tuningParameters['memoryRequested']:
                fileAttributes.errors.append(f"{len(fileAttributes.errors)+1}: Off heap(memory requested - Xmx) size must be less than 50% of requested memory")
                # raise AssertionError("Off heap size must be less than 50% of requested memory")
    # Cost Analysis
    # performCostAnalysis(fileAttributes)   

def performCostAnalysis(fileAttributes: YamlAttributes):
    machine = fileAttributes.machine
    tuningParameters = fileAttributes.tuningParameters
    if 'cpu' in machine and 'memory' in machine and 'cost' in machine:
        replicaCount = 1
        if 'maxReplicaCount' in tuningParameters:
            replicaCount = tuningParameters['maxReplicaCount']
        elif 'replicas' in tuningParameters:
            replicaCount = tuningParameters['replicas']
        
        machineCPU = UnitHandler.getCPUsInCores(machine['cpu'], fileAttributes)
        machineMemory = UnitHandler.getMemorySizeInGigabytes(machine['memory'], fileAttributes)
        machineRatio = float("{:.4f}".format(machineMemory/machineCPU))

        if 'memoryToCPURatio' in tuningParameters:

            podRatio = tuningParameters['memoryToCPURatio']

            if podRatio <= machineRatio:
                # CPU gets exhausted first, cost according to cpu
                tuningParameters['podCost'] = replicaCount * tuningParameters['CPURequested'] * (machine['cost']/machineCPU)
            else:
                # Memory gets exhausted first, cost according to memory
                tuningParameters['podCost'] = replicaCount * tuningParameters['memoryRequested'] * (machine['cost']/machineMemory)
        elif 'limitingMemory' in tuningParameters:
            tuningParameters['podCost'] = replicaCount * tuningParameters['limitingMemory'] * (machine['cost']/machineMemory)
        elif 'limitingCPU' in tuningParameters:
            tuningParameters['podCost'] = replicaCount * tuningParameters['limitingCPU'] * (machine['cost']/machineCPU)

        fileAttributes.tuningParameters = tuningParameters

def getMachine(configParams: dict, fileAttributes: YamlAttributes) -> bool:
    tuningParameters = fileAttributes.tuningParameters

    if 'cluster' in tuningParameters:
        if tuningParameters['cluster'] in configParams['clusters']:
            if 'nodeSelectorKey' in tuningParameters and tuningParameters['nodeSelectorKey'] in configParams['clusters'][tuningParameters['cluster']]:
                if 'nodeSelectorValue' in tuningParameters and tuningParameters['nodeSelectorValue'] in configParams['clusters'][tuningParameters['cluster']][tuningParameters['nodeSelectorKey']]:
                    machine = configParams['clusters'][tuningParameters['cluster']][tuningParameters['nodeSelectorKey']][tuningParameters['nodeSelectorValue']]

                    # Handle multiple machines here
                    # CPU to memory ratio would ideally be same for all the machines.
                    targetMachine = machine
                    machineDict = {
                        'cpu': configParams['instanceTypes'][targetMachine]['cpu'],
                        'memory': configParams['instanceTypes'][targetMachine]['memory'],
                        'cost': float(configParams['instanceTypes'][targetMachine]['cost']),
                    }
                    # # Set the cost as the average of all
                    # if len(machines) > 1:
                    #     cost = 0
                    #     for machine in machines:
                    #         cost += float("{:.4f}".format(float(configParams['instanceTypes'][machine]['cost'])))

                    #     cost = float("{:.4f}".format(cost/len(machines)))
                    #     machineDict['cost'] = cost
                    fileAttributes.machine = machineDict
                    return True
    return False

def getErrMessage(errors: list, filename: str):
    errMessage = "\n"
    for err in errors:
        errMessage = errMessage + err + "\n"
    
    return f"\n{filename}: {errMessage}\n"

def parseYamls(lines: list, isCostCheck=False) -> dict:
        changedFilesAttributes = {}
        for line in lines:
            # Check if the file is a yaml file.
            ymlName = str(line)[:-1]
            if ymlName[-10:] == "values.yml" or ymlName[-11:] == "values.yaml":
                if os.path.exists(ymlName):
                    with open(ymlName, 'r') as f:
                        fileAttributes = YamlAttributes(ymlName)
                        # print(f"[*] Parsing {ymlName}:")
                        parsedYaml = parseWithErrorOnDuplicateKeys(f, fileAttributes)
                        if isCostCheck:
                            getNodeParams(parsedYaml, fileAttributes.tuningParameters)
                            if getMachine(configParams, fileAttributes):
                                getReplicasAndAutoScaleParams(parsedYaml, fileAttributes)
                                getMemoryAndCPU(parsedYaml, fileAttributes, isCostCheck=True)
                                calculateRatios(fileAttributes)
                                performCostAnalysis(fileAttributes)
                        else:
                            getNodeParams(parsedYaml, fileAttributes.tuningParameters)
                            getMachine(configParams, fileAttributes)
                            getParameters(parsedYaml, fileAttributes, isCostCheck)
                            calculateRatios(fileAttributes)
                            performChecks(fileAttributes, configParams)
                        changedFilesAttributes[fileAttributes.filename] = fileAttributes
        return changedFilesAttributes

if __name__ == "__main__":
    args = getCMDArgs()
    lines = None
    with open("changed_files.txt", "r") as fp:
        lines = fp.readlines()
    
    with open("pipeline/config.yml", "r") as fp:
        configParams = yaml.load(fp, Loader=BaseLoader)
    
    with open("pipeline/javaopts.txt", "r") as fp:
        validJavaOpts = set()
        for line in fp.readlines():
            validJavaOpts.add(line.strip('\n'))

    if args.cost:
        # Run for the target branch
        os.system(f'git checkout {args.target_branch} >/dev/null 2>&1')
        targetBranchChangedFilesAttributes = parseYamls(lines, isCostCheck=True)
        os.system('git restore .')
        # Run for source branch
        os.system(f'git checkout {args.source_branch} >/dev/null 2>&1')
        sourceBranchChangedFilesAttributes = parseYamls(lines, isCostCheck=True)

        errMessageAllFiles = ""
        totalCostImpact = 0
        costDict = {}
        for filename in sourceBranchChangedFilesAttributes.keys():
            if filename in targetBranchChangedFilesAttributes.keys():
                # print(f"Cost analysis for {filename}")
                sourceBranchAttributes = sourceBranchChangedFilesAttributes[filename]
                targetBranchAttributes = targetBranchChangedFilesAttributes[filename]
                # print(sourceBranchAttributes.tuningParameters)
                # print(targetBranchAttributes.tuningParameters)
                if 'podCost' in sourceBranchAttributes.tuningParameters:
                    sourceBranchCost = sourceBranchAttributes.tuningParameters['podCost']
                    if 'podCost' in targetBranchAttributes.tuningParameters:
                        
                        targetBranchCost = targetBranchAttributes.tuningParameters['podCost']

                        totalCostImpact += sourceBranchCost - targetBranchCost
                        
                        if sourceBranchCost > targetBranchCost:
                            if sourceBranchCost-targetBranchCost > int(configParams['diffPriceThreshold']):
                                diff = "{:.2f}".format(sourceBranchCost-targetBranchCost)
                                # errMessageAllFiles += f"\n{filename} -> cost ${diff}\n"
                                try:
                                    costDict[int(float(diff))].append(filename)
                                except:
                                    costDict[int(float(diff))] = [filename]
                                # raise AssertionError(f"Cost difference {sourceBranchCost-targetBranchCost} exceeds diffPriceThreshold. Merge increases cost.")
                        elif sourceBranchCost < targetBranchCost:
                            if targetBranchCost-sourceBranchCost > int(configParams['diffPriceThreshold']):
                                diff = "{:.2f}".format(targetBranchCost-sourceBranchCost)
                                # errMessageAllFiles += f"\n{filename} -> cost ${diff}\n"
                                try:
                                    costDict[int(float(diff))].append(filename)
                                except:
                                    costDict[int(float(diff))] = [filename]
                                # raise AssertionError(f"Cost difference {targetBranchCost-sourceBranchCost} exceeds diffPriceThreshold. Merge decreases cost.")
                    else:
                        # Maybe a fix in the cluster name, leading to pod cost calculation possible now
                        totalCostImpact += sourceBranchCost
                        if sourceBranchCost > int(configParams['newDeploymentPriceThreshold']):
                            cost = "{:.2f}".format(sourceBranchCost)
                            # errMessageAllFiles += f"\n{filename} -> cost ${cost}\n"
                            try:
                                costDict[int(float(cost))].append(filename)
                            except:
                                costDict[int(float(cost))] = [filename]
            else:
                # May include new deployment
                sourceBranchAttributes = sourceBranchChangedFilesAttributes[filename]
                # print(sourceBranchAttributes.tuningParameters)
                if 'podCost' in sourceBranchAttributes.tuningParameters:
                    totalCostImpact += sourceBranchAttributes.tuningParameters['podCost']
                    if sourceBranchAttributes.tuningParameters['podCost'] > int(configParams['newDeploymentPriceThreshold']):
                        cost = "{:.2f}".format(sourceBranchAttributes.tuningParameters['podCost'])
                        # errMessageAllFiles += f"\n{filename} -> cost ${cost}\n"
                        try:
                            costDict[int(float(cost))].append(filename)
                        except:
                            costDict[int(float(cost))] = [filename]
                        # raise AssertionError(f"{filename} cost {sourceBranchAttributes.tuningParameters['podCost']} exceeds newDeploymentPriceThreshold.")
        print(f"\n\n\n[*] Total cost impact: ${int(float('{:.2f}'.format(totalCostImpact)))}\n\n\n")
        if len(costDict) > 0:
            costs = list(costDict.keys())
            costs.sort(reverse=True)

            for cost in costs:
                for filename in costDict[cost]:

                    print(f"{filename} -> ${cost}\n")
            sys.exit(1)
    else:
        sourceBranchChangedFilesAttributes = parseYamls(lines, isCostCheck=False)
        errMessageAllFiles = ""
        for filename in sourceBranchChangedFilesAttributes.keys():
            if len(sourceBranchChangedFilesAttributes[filename].errors) > 0:
                errMessageAllFiles += getErrMessage(sourceBranchChangedFilesAttributes[filename].errors, filename)
            # print(f"{filename}: {sourceBranchChangedFilesAttributes[filename].tuningParameters}")
        if len(errMessageAllFiles) > 0:
            print(f"Errors:\n {errMessageAllFiles}")
            sys.exit(1)