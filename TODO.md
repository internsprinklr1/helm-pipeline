# TODO:
- [X] See the default values that are assigned if CPU, memory or heap sizes are not defined.
    * If requests are not assigned, but limit is defined, allocated = limit
    * If limit not assigned, limit = allocated = capacity of the node
    * Admins can also set a default for a particular namespace by deploying a `LimitRange` object on the cluster
    * Default `Xmx` is **1/4th of physical memory**, default `Xms` is **1/64th of physical memory**
- [X] What to use for CPU to memory ratio? Limits or requests or heap or what?
    * I think limit should be used, as that is the max size to which the pod can expand.
- [X] Usually the only file to check is `values.yml`, update script accordingly.
- [X] Make a yaml config file that contains:
    1. The rules the script needs to check for.
    2. CPU and memory specifications of the cloud instances used.
- [X] Get k8s cluster name from the values.yml file
- [X] Get machine specifications from node selector
- [X] For multiple machines, get the price as average
- [X] Compute costs
- [X] Add a different job for cost analysis
- [ ] Show units of cost, fix decimal points
- [ ] Find out any syntax errors in java opts
