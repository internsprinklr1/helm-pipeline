FROM python:3.12.0b3-alpine3.18

WORKDIR /app

COPY requirements.txt .

RUN apk add git; pip install -r requirements.txt

CMD [ "/bin/sh" ]